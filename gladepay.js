jQuery(function($) {
    var checkout_form = $("form.woocommerce-checkout");

    //hide and show the place order button based on the current checkout method enabled
    option = $("input[name='payment_method']:checked").val();

    if (option == "gladepay") {
        // $('#place_order').hide();
        $(".payment_box.payment_method_gladepay").show();
    } else {
        // $('#place_order').show();
        $(".payment_box.payment_method_gladepay").hide();
    }

    $("input[name='payment_method']").change(function() {
        option = $("input[name='payment_method']:checked").val();

        if (option == "gladepay") {
            // $('#place_order').hide();
            $(".payment_box.payment_method_gladepay").show();
        } else {
            // $('#place_order').show();
            $(".payment_box.payment_method_gladepay").hide();
        }
    });

    $("#place_order").click(function(event) {
        if (
            jQuery("#place_order")
            .text()
            .toLowerCase() != "complete order"
        ) {
            event.preventDefault();

            initPayment({
                MID: gladepay_params.merchant_key,
                email: checkout_form.find("#billing_email").val(),
                firstname: checkout_form.find("#billing_first_name").val(),
                lastname: checkout_form.find("#billing_last_name").val(),
                description: gladepay_params.description,
                title: gladepay_params.title,
                amount: gladepay_params.total,
                country: "NG",
                currency: gladepay_params.currency,
                onclose: function() {
                    console.log("failed request");
                },
                callback: function(data) {
                    var response = data;
                    if (
                        response.status == 200 &&
                        response.txnStatus.toLowerCase() == "successful"
                    ) {
                        checkout_form.find("#txnRef").val(response.txnRef);

                        $("#gladepay-payment-section").html(
                            "<div style='font-weight:bold; color: #21c91c; font-size:14px'>Payment Complete, Complete your order below...</div>"
                        );
                        $("#place_order").show();
                        $("#place_order").html("Complete Order");

                        //trigger the place order button or allow the user to trigger it them self
                        $("#place_order").click();
                    } else {
                        alert("Payment Failed: " + response.bank_message);
                    }
                }
            });
        }
    });
});